/*
* B31.c
*
* Created: 2-3-2016 10:39:04
* Author : guusv_000
*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define BIT(x)	(1 << (x))

// wait(): busy waiting for 'ms' millisecond
// Used library: util/delay.h
void wait( int ms )
{
	for (int tms=0; tms<ms; tms++)
	{
		_delay_ms( 1 );			// library function (max 30 ms at 8MHz)
	}
}


// Initialize ADC
void adcInit( void )
{
	ADMUX = 0b11100011;			// AREF=2,56 V, result left adjusted, channel3 at pin PF3
	ADCSRA = 0b10000110;		// ADC-enable, no interrupt, no free running, division by 64
}


// Main program: ADC at PF3
int main( void )
{
	DDRF = 0x00;				// set PORTF for input (ADC)
	DDRA = 0xFF;				// set PORTA for output
	DDRB = 0xFF;				// set PORTB for output
	DDRC = 0x00;				// set PORTC for input (Pins)
	adcInit();					// initialize ADC

	while (1)
	{
		ADCSRA |= BIT(6);				// Start ADC
		while ( ADCSRA & BIT(6) ) ;		// Wait for completion
		PORTA = ADCH;           // Show MSB (bit 9:2) of ADC
		wait(100);				// every 100 ms (busy waiting)
	}
}


