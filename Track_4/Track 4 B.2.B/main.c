/*/*
 * Project name:
     Demo4_5 : PWM with timer 1 Fast PWM mode at PORTB.5, PB.6 en PB.7
	 			parallel, 3x, for RGB LED
 * Author: Avans-TI, JW
 * Revision History: 
     20101230: - initial release;
 * Description:
     This program gives an interrupt on each ms
 * Test configuration:
     MCU:             ATmega128
     Dev.Board:       BIGAVR6
     Oscillator:      External Clock 08.0000 MHz
     Ext. Modules:    -
     SW:              AVR-GCC
 * NOTES:
     - Turn ON the PORT LEDs at SW12.1 - SW12.8
*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define BIT(x)			(1 << (x))

unsigned int sCount=0, minutes=0, hours=0;

// wait(): busy waiting for 'ms' millisecond
// Used library: util/delay.h
void wait( int ms )
{
	for (int tms=0; tms<ms; tms++)
	{
		_delay_ms( 1 );			// library function (max 30 ms at 8MHz)
	}
}

// Initialize timer 1: fast PWM at pin PORTB.6 (hundredth ms)
void timer1Init( void )
{
	OCR1A = 0;					// RED, default, off
	OCR1B = 0;					// GREEN, default, off
	OCR1C = 0;					// BLUE, default, off
	TCCR1A = 0b10101001;		// compare output OC1A,OC1B,OC1C
	TCCR1B = 0b00001011;		// fast PWM 8 bit, prescaler=64, RUN
}

// Set pulse width for RED on pin PB5, 0=off, 255=max
void setRed( unsigned char red )
{
	OCR1A = red;
}

 void setGreen( unsigned char green)
 {
	OCR1B = green;
 }
 void setBlue( unsigned char blue)
 {
	OCR1C = blue;
 }

// Main program: Counting on T1
int main( void )
{
	DDRB = 0xFF;					// set PORTB for compare output
	timer1Init();
	wait(100);
	int deltaR = 1;
	int deltaB = 1;
	int deltaG = 1;
	///* increase blue*/
	//for (int blue = 0; blue<=255; blue+=deltaB)
	//{
		//setBlue( blue );				// 8-bits PWM on pin OCR1a
		//deltaB += 2;					// progressive steps down
		//wait(200);					// delay of 100 ms (busy waiting)
	//}
	wait(200);
	while (1)
	{
		//setRed (0);

		/* increase red */
		for (int red = 0; red<=255; red+=deltaR)
		{
			setRed( red );				// 8-bits PWM on pin OCR1a 
			deltaR += 2;					// progressive steps up
			wait(200);					// delay of 100 ms (busy waiting)
		}
		wait(200);
		/* decrease red */
		for (int red = 255; red>=0; red-=deltaR)
		{
			setRed( red );				// 8-bits PWM on pin OCR1a
			deltaR -= 2;					// progressive steps down
			wait(200);					// delay of 100 ms (busy waiting)
		}
		//setRed(0);
		wait(200);
		/* increase blue*/
		for (int blue = 0; blue<=255; blue+=deltaB)
		{
			setBlue( blue );				// 8-bits PWM on pin OCR1a
			deltaB += 2;					// progressive steps down
			wait(200);					// delay of 100 ms (busy waiting)
		}
		wait(200);
		/* decrease blue */
		for (int blue = 255; blue>=0; blue-=deltaB)
		{
			setBlue( blue );				// 8-bits PWM on pin OCR1a
			deltaB -= 2;					// progressive steps down
			wait(200);					// delay of 100 ms (busy waiting)
		}
		//setBlue(0);
		wait(200);
		/* increase green */
		for (int green = 0; green<=255; green+=deltaG)
		{
			setGreen( green );				// 8-bits PWM on pin OCR1a
			deltaG += 2;					// progressive steps up
			wait(200);					// delay of 100 ms (busy waiting)
		}
		wait(200);
		/* decrease green */
		for (int green = 255; green>=0; green-=deltaG)
		{
			setGreen( green );				// 8-bits PWM on pin OCR1a
			deltaG -= 2;					// progressive steps down
			wait(200);					// delay of 100 ms (busy waiting)
		}
		wait(200);
		//setGreen(0);
	} 
}