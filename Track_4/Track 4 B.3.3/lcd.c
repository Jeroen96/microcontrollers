/*
* lcd.c
*
* Created: 24-2-2016 10:26:42
*  Author: Jeroen
*/

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

void init_lcd(void);
void lcd_writeChar( unsigned char dat );
void lcd_command( unsigned char dat );
void lcd_writeText(char *text);

void init_lcd(void)
{
	DDRC = 0xFF;
	PORTC = 0x00;

	PORTC = 0x20; //enable 4-bit mode
	//*********** E=1 & E=0
	PORTC = 0x08;
	_delay_ms(1);
	PORTC = 0x00;
	_delay_ms(1);
	//******************

	// mode: 4 bits interface data, 2 lines, 5x8 dots
	lcd_command( 0x28 );
	// display: on, cursor off, blinking off
	lcd_command( 0x0C );
	// entry mode: cursor to right, no shift
	lcd_command( 0x06 );
	// RAM adress: 0, first position, line 1
	lcd_command( 0x80 );
}

void lcd_writeChar( unsigned char dat )
{
	PORTC = dat & 0xF0; // hoge nibble
	PORTC = PORTC | 0x0C; // data (RS=1),
	// start (EN=1)
	_delay_ms(1); // wait 1 ms
	PORTC = 0x04; // stop (EN = 0)

	PORTC = (dat & 0x0F) << 4; // lage nibble
	PORTC = PORTC | 0x0C; // data (RS=1),
	// start (EN=1)
	_delay_ms(1); // wait 1 ms
	PORTC = 0x00; // stop
	// (EN=0 RS=0)
}

void lcd_command ( unsigned char dat )
{
	PORTC = dat & 0xF0; // hoge nibble
	PORTC = PORTC | 0x08; // data (RS=0),  E= 4 RS =3
	// start (EN=1)
	_delay_ms(1); // wait 1 ms
	PORTC = 0x00; // stop (EN = 0)

	PORTC = (dat & 0x0F) << 4; // lage nibble
	PORTC = PORTC | 0x08; // data (RS=0),
	// start (EN=1)
	_delay_ms(1); // wait 1 ms
	PORTC = 0x00; // stop
	// (EN=0 RS=0)
}

/* Funtion only works with letters not with numbers */
void lcd_writeText(char *text){
	lcd_command(0x80);
	for(int i = 0; i<strlen(text);i++){
		lcd_writeChar(text[i]);
	}
}


void shiftR(void){
	lcd_command(0x1C);
}

void shiftL(void){
	lcd_command(0x18);
}

/* int line2(used as boolean) 0 = first line , 1 = second line */
void moveCursorR(int position,int line2){
	int i = 0;
	if(line2){
		lcd_command(0xC0); //Set to adress 64(first on row 2)
		while(i< position){
			lcd_command(0x14);
			i++;
		}
		}else{
		while(i<position){
		lcd_command(0x80); // Set adress to 0
			lcd_command(0x14);//Shifts the cursor position to the right. (AC is incremented by one.)
			i++;
		}
	}
}

/* int line2(used as boolean) 0 = first line , 1 = second line */
void moveCursorL(int position,int line2){
	int i = 0;
	if(line2){
		lcd_command(0xC0);	//Set to adress 64(first on row 2)
		while(i< position){
			lcd_command(0x10);
			i++;
		}
		}else{
		while(i<position){
			lcd_command(0x80); // Set to addres 0
			lcd_command(0x10);//Shifts the cursor position to the right. (AC is incremented by one.)
			i++;
		}
	}
}