/*
* LcdB1.c
*
* Created: 23-2-2016 10:19:48
* Author : guusv_000
*/

#include <avr/io.h>
#include <util/delay.h>
#include <math.h>
#include "lcd.h"

#define BIT(x)	(1 << (x))

void Lcd_WriteFirstLine(char * text){
	lcd_command(0x80);
	while(*text) {
		lcd_writeChar(*text++);
	}
}


void Lcd_WriteSecondLine(char * text){
	set_cursor(0xC0);
	while(*text) {
		lcd_writeChar(*text++);
	}
}

void wait( int ms )
{
	for (int tms=0; tms<ms; tms++)
	{
		_delay_ms( 1 );			// library function (max 30 ms at 8MHz)
	}
}

int binary_decimal(int n) /* Function to convert binary to decimal.*/
{
	int decimal=0, i=0, rem;
	while (n!=0)
	{
		rem = n%10;
		n/=10;
		decimal += rem*pow(2,i);
		++i;
	}
	return decimal;
}

void adcInit( void )
{
	ADMUX = 0b11000001;			// AREF=2.56V, result left adjusted, channel1 at pin PF1
	ADCSRA = 0b11100110;		// ADC-enable, no interrupt, start, free running, division by 64
}


// Main program: ADC at PF1
int main( void )
{
	DDRF = 0x00;				// set PORTF for input (ADC)
	DDRA = 0xFF;				// set PORTA for output
	DDRB = 0xFF;				// set PORTB for output

	adcInit();					// initialize ADC
	init_lcd();						// initialize LCD
	char str[40];
	while (1)
	{
		sprintf(str, "Temp is %d", binary_decimal(ADCL)); // When using ADCH avg temp is 12/13 degrees(with ADLAR = 1)(ADCL gives 16 or 24 and glitches)   #### With ADCL avg temp is 20-25(with ADLAR = 0) /ADCH gives glitches and no temp
		PORTB = ADCL;			// Show MSB/LSB (bit 10:0) of ADC
		PORTA = ADCH;
		lcd_writeText(str);
		wait(250);				// every 100 ms (busy waiting)
		lcd_command(0x01); //Clear lcd
	}
}
