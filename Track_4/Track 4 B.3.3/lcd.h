/*
 * lcd.h
 *
 * Created: 24-2-2016 10:27:03
 *  Author: Jeroen
 */ 


#ifndef LCD_H_
#define LCD_H_

extern void init_lcd(void);
extern void lcd_writeChar( unsigned char dat );
extern void lcd_command ( unsigned char dat );
extern void lcd_writeText(char *text);
extern void shiftR(void);
extern void shiftL(void);
extern void moveCursorL(int position,int line2);
extern void moveCursorR(int position,int line2);
#endif /* LCD_H_ */