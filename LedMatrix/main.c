/* ---------------------------------------------------------------------------
** This software is in the public domain, furnished "as is", without technical
** support, and with no warranty, express or implied, as to its usefulness for
** any purpose.
**
** ledmatrix.c
**
** Beschrijving:	Simple HT16K33 Ledmatix demo.
** Target:			AVR mcu
** Build:			avr-gcc -std=c99 -Wall -O3 -mmcu=atmega128 -D F_CPU=8000000UL -c ledmatrix.c
**					avr-gcc -g -mmcu=atmega128 -o ledmatrix.elf ledmatrix.o
**					avr-objcopy -O ihex ledmatrix.elf ledmatrix.hex 
**					or type 'make'
** Author: 			dkroeske@gmail.com
** -------------------------------------------------------------------------*/


#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>

int * p;

int hoi[]= {  
			0b00000000000000,
			0b00000000000000,
			0b11101111010010,
			0b01001001010010,
			0b01001001011110,
			0b01001001010010,
			0b11101111010010,
			0b00000000000000,
		 };

/*int nerdsAreTheNewCool[] = {
								0b000000000000000000,
								0b0000000000000000000,
								0b0111001110111011110,
								0b1001010010001010010,0
								0b1001000010111010010,
								0b1001000010001010010,
								0b0111000010111010010,
								0b0000000000000000000,
						   }*/


/******************************************************************/
void twi_init(void)
/* 
short:			Init AVR TWI interface and set bitrate
inputs:			
outputs:	
notes:			TWI clock is set to 100 kHz
Version :    	DMK, Initial code
*******************************************************************/
{
	TWSR = 0;
	TWBR = 32;	 // TWI clock set to 100kHz, prescaler = 0
}

/******************************************************************/
void twi_start(void)
/* 
short:			Generate TWI start condition
inputs:		
outputs:	
notes:			
Version :    	DMK, Initial code
*******************************************************************/
{
	TWCR = (0x80 | 0x20 | 0x04);
	while( 0x00 == (TWCR & 0x80) );
}

/******************************************************************/
void twi_stop(void)
/* 
short:			Generate TWI stop condition
inputs:		
outputs:	
notes:			
Version :    	DMK, Initial code
*******************************************************************/
{
	TWCR = (0x80 | 0x10 | 0x04);
}

/******************************************************************/
void twi_tx(unsigned char data)
/* 
short:			transmit 8 bits data
inputs:		
outputs:	
notes:			
Version :    	DMK, Initial code
*******************************************************************/
{
	TWDR = data;
	TWCR = (0x80 | 0x04);
	while( 0 == (TWCR & 0x80) );
}

/******************************************************************/
void wait( int ms )
/* 
short:			Busy wait number of millisecs
inputs:			int ms (Number of millisecs to busy wait)
outputs:	
notes:			Busy wait, not very accurate. Make sure (external)
				clock value is set. This is used by _delay_ms inside
				util/delay.h
Version :    	DMK, Initial code
*******************************************************************/
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );		// library function (max 30 ms at 8MHz)
	}
}

void display(unsigned char data, unsigned char address){
	twi_start();
	twi_tx(0xE0);	// Display I2C addres + R/W bit
	twi_tx(address);	// Address
	twi_tx(data);	// data
	twi_stop();
}

void clearDisplay(){
	display(0x00, 0x00);
	display(0x00, 0x02);
	display(0x00, 0x04);
	display(0x00, 0x06);
	display(0x00, 0x08);
	display(0x00, 0x0A);
	display(0x00, 0x0C);
	display(0x00, 0x0E);
}

int * intdup(int const * src, int len)
{
	p = malloc(len * sizeof(int));
	memcpy(p, src, len * sizeof(int));
	return p;
}

/******************************************************************/
int main( void )
/* 
short:			main() loop, entry point of executable
inputs:			
outputs:	
notes:			Looping forever, trashing the HT16K33
Version :    	DMK, Initial code
*******************************************************************/
{
	int i = 0;
	twi_init();		// Init TWI interface

	// Init HT16K22. Page 32 datasheet
	twi_start();
	twi_tx(0xE0);	// Display I2C addres + R/W bit
	twi_tx(0x21);	// Internal osc on (page 10 HT16K33)
	twi_stop();

	twi_start();
	twi_tx(0xE0);	// Display I2C address + R/W bit
	twi_tx(0xA0);	// HT16K33 pins all output
	twi_stop();

	twi_start();
	twi_tx(0xE0);	// Display I2C address + R/W bit
	twi_tx(0xE3);	// Display Dimming 4/16 duty cycle
	twi_stop();

	twi_start();
	twi_tx(0xE0);	// Display I2C address + R/W bit
	twi_tx(0x81);	// Display OFF - Blink On
	twi_stop();

	while (1)
	{
		clearDisplay();
		int*copy; 
		copy = intdup(hoi, 8);
		free(p);


		for (int idx = 0; idx < 8; idx++){
			int data = copy[idx];
			int x = (data >> i) & 1;
			data = copy[idx]>>1;
			if (x != 1){
				data &= ~(1 << (i+7));
			} else {
				data |= 1 << (i+7);
			}
			copy[idx] = data;
		}

		display(copy[0]>>i, 0x00);
		display(copy[1]>>i, 0x02);
		display(copy[2]>>i, 0x04);
		display(copy[3]>>i, 0x06);
		display(copy[4]>>i, 0x08);
		display(copy[5]>>i, 0x0A);
		display(copy[6]>>i, 0x0C);
		display(copy[7]>>i, 0x0E);
		i++;

		if (i > 14){
			i = 0;
		}
		wait(1000);
	}

	return 1;
}