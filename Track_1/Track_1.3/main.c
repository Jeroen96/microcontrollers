/*
 * Track_1.3.c
 *
 * Created: 21-2-2016 20:03:11
 * Author : Jeroen
 */ 

#include <avr/io.h>
#include <util/delay.h>

void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );
	}
}


int main(void)
{
	/* Replace with your application code */
	DDRD = 0xFF;
	DDRC = 0x00;

	while (1)
	{
		if (PINC & 0x01)
		{
			PORTD = 0x40;
			wait(250);
			PORTD = 0x00;
			wait(250);
		}
		else
		{
			PORTD = 0x00;				// write 0 to all the bits of PortD
		}
	}
	return 1;
}

