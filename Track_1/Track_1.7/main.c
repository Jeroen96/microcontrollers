/*
 * Track_1.7.c
 *
 * Created: 21-2-2016 20:04:54
 * Author : Jeroen
 */ 

#include <avr/io.h>
#include <util/delay.h>

void s1(void);
void s2(void);
void s3(void);
void start(void);
void end(void);

void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );
		
	}
}

typedef enum {
	ST_S1,
	ST_S2,
	ST_S3,
	ST_START,
	ST_END
}STATES;

typedef enum{
	D5,
	D6,
	D7
}EVENTS;

STATES state = ST_START;

// Sample fsm definition:
//
// 		| EV_D7    EV_D6		EV_D5
// ---------------------------------
// START| START     S1          S2
// S1   | START     S1        	S2
// S2   | START     S1          S3
// S3   | START     END         END
// END  | START     END         END

void handleEvent(EVENTS event){
	switch (state) //Check current state
	{
		case ST_START:
			switch(event){
				case D5:
					s2();
					break;
				case D6:
					s1();
					break;
				case D7:
					start();
					break;
			}
			break;
		case ST_S1:
			switch(event){
				case D5:
					s2();
					break;
				case D6:
					s1();
					break;
				case D7:
					start();
					break;
			}
			break;
		case ST_S2:
			switch(event){
				case D5:
					s3();
					break;
				case D6:
					s1();
					break;
				case D7:
					start();
					break;
			}
			break;
		case ST_S3:
			switch(event){
				case D5:
					end();
					break;
				case D6:
					end();
					break;
				case D7:
					start();
					break;
			}
			break;
		case ST_END:
			switch(event){
				case D5:
					end();
					break;
				case D6:
					end();
					break;
				case D7:
					start();
					break;
			}
			break;
	}
}


//State 1
void s1(void){
	PORTC = 0x01; //Light 0 on
	wait(500); //Wait 0.5 sec to seperate states.
	state = ST_S1; //Update state
}
//State 2
void s2(void){
	PORTC = 0x02; //Light 1 on
	wait(500); //Wait 0.5 sec to seperate states.
	state = ST_S2; //Update state
}
//State 3
void s3(void){
	PORTC = 0x04; //Light 2 on
	wait(500); //Wait 0.5 sec to seperate states.
	state = ST_S3; //Update state	
}
//State start
void start(void){
	PORTC = 0x30; //Light 4 & 5 on
	wait(500); //Wait 0.5 sec to seperate states.
	state = ST_START; //Update state
}
//State end
void end(void){
	PORTC = 0xC0; //Light 6 & 7 on
	wait(500); //Wait 0.5 sec to seperate states.
	state = ST_END; //Update state
}

int main(void)
{
	DDRD = 0x0F; //Pin PORTD<7:4> input,PORT<3:0> output
	DDRC = 0xFF; //Pin PORTC<7:0> output    
	
	handleEvent(D6);
	handleEvent(D5);
	handleEvent(D5);
	handleEvent(D6);
	handleEvent(D7);
	
	while (1) 
    {
    }
}

