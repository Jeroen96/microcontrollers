/*
 * Track_1.4.c
 *
 * Created: 21-2-2016 20:03:31
 * Author : Jeroen
 */ 

#include <avr/io.h>
#include <util/delay.h>

void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );
	}
}

int main( void )
{
	
	DDRD = 0b11111111;
	int hex = 0x01;
	while (1)
	{
		for(int i = 0; i<8; i++){
			PORTD = hex;
			hex = hex << 1;
			wait(2000);
		}
		hex = 0x01;
	}
	return 1;
}

