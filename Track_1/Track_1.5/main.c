/*
 * Track_1.5.c
 *
 * Created: 21-2-2016 20:03:52
 * Author : Jeroen
 */ 

#include <avr/io.h>
#include <util/delay.h>

typedef struct{
	unsigned int hex;
	unsigned int delay;
}command;

command pattern[] = {{0x01,1000},{0x02,1000},{0x04,1000},{0x08,1000},{0x10,1000},{0x20,1000},{0x40,1000},{0x80,1000},
{0x40,1000},{0x20,1000},{0x10,1000},{0x08,1000},{0x04,1000},{0x02,1000},{0x01,1000},
{0x18,1000},{0x24,1000},{0x42,1000},{0x81,1000},
{0x81,1000},{0x42,1000},{0x24,1000},{0x18,1000}
};


void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );
	}
}

int main(void)
{
	DDRD = 0b11111111;

	while (1)
	{
		int index = 0;
		while(index < (sizeof(pattern)/sizeof(pattern[0])))
		{
			PORTD = pattern[index].hex;
			wait(pattern[index].delay);
			index++;
		}
	}
	return 1;
}

