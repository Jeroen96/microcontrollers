/*
 * Track_1.6.c
 *
 * Created: 21-2-2016 20:04:13
 * Author : Jeroen
 */ 

#include <avr/io.h>
#include <util/delay.h>

int time = 1000;

void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );		// library function (max 30 ms at 8MHz)
		
	}
}


int main(void)
{
	int time = 1000;
	DDRC = 0x00;
	DDRD = 0xFF;
	while (1)
	{
		if(PINC & 0x01){
			if(time == 1000){
				time = 250;
				}else{
				time = 1000;
			}
		}
		PORTD = 0x80;
		wait(time);
		PORTD = 0x00;
		wait(time);
	}
}

