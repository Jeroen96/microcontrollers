/*
 * Track_1.2.c
 *
 * Created: 21-2-2016 20:01:23
 * Author : Jeroen
 */ 

#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
	DDRD = 0b11111111;					// PORTD.7 input all other bits output
	
	while (1)
	{
		PORTD = 0x40;
		wait(1000);
		PORTD = 0x80;
		wait(1000);
	}

	return 1;
}

void wait(int ms)
{
	for(int i=0; i<ms; i++)
	{
		_delay_ms(1);
	}
}

