/*
 * LcdB1.c
 *
 * Created: 23-2-2016 10:19:48
 * Author : guusv_000
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include "lcd.h"


void Lcd_WriteFirstLine(char * text){
	set_cursor(0x80);
	while(*text) {
		lcd_writeChar(*text++);
	}
}


void Lcd_WriteSecondLine(char * text){
	set_cursor(0xC0);
	while(*text) {
		lcd_writeChar(*text++);
	}
}

int main(void)
{
	DDRC = 0xFF; //PORTC als ouput
	DDRD = 0xFF; //PORTD als output

	init(); //Initializeert LCD scherm in 4 bit mode

	Lcd_WriteFirstLine("test123            "); //schrijft char array naar LCD scherm
	Lcd_WriteSecondLine("test567            ");
	while(1){
		PORTD ^= (1<<7);	// Toggle PORTD.7
		_delay_ms( 250 );   // Delay voor het knipperen
	}
}

