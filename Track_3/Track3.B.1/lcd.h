/*
 * lcd.h
 *
 * Created: 23-2-2016 10:21:04
 *  Author: guusv_000
 */ 


#ifndef LCD_H_
#define LCD_H_

extern void init();
extern void display_text(unsigned char dat);
extern void lcd_writeChar(unsigned char dat);
extern void set_cursor(int position);
extern void shiftR(void);
extern void shiftL(void);
extern void moveCursorL(int position,int line2);
extern void moveCursorR(int position,int line2)
#endif /* LCD_H_ */