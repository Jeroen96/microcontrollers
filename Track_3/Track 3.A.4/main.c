/*
* A.4.c
*
* Created: 24-2-2016 10:23:48
* Author : Jeroen
*/

#include <avr/io.h>
#include <util/delay.h>
#include "lcd.h"

int main(void)
{
	/* Replace with your application code */

	DDRD = 0xFF;
	init_lcd();
	lcd_writeText("Micro, A.4");
	
	while (1)
	{
		for(int i=0 ; i<6; i++){
			shiftR();
			_delay_ms(800);
		}
		for(int i=0;i<6;i++){
			shiftL();
			_delay_ms(800);
		}
	}
}

