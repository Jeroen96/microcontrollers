/*
 * CounterB2.c
 *
 * Created: 26-2-2016 11:46:58
 * Author : guusv_000
 */ 
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "lcd.h"

unsigned int counter = 0;


void Lcd_WriteFirstLine(char * text){
	set_cursor(0x80);
	while(*text) {
		lcd_writeChar(*text++);
	}
}


void Lcd_WriteSecondLine(char * text){
	set_cursor(0xC0);
	while(*text) {
		lcd_writeChar(*text++);
	}
}



ISR( TIMER2_OVF_vect )
{
	counter++;			// Increment counter
	TCNT2 = -2;
}

// Initialize timer2
//
void timer2Init( void )
{
	TCNT2 = -2;				// of  TCNT2=0xf6
	TIMSK |= (1 << TOIE2);			// T2 overflow interrupt enable, p. 162
	//TCCR2 = 0b00011111;		// counter, normal mode, run
	TCCR2 |= (1 << CS20) | (1 << CS21) | (1 << CS22) | (1 << WGM21) | (1 << COM20);
}



int main(void)
{
	DDRC = 0xFF; //PORTC als ouput
	DDRD = 0b00000000;

	timer2Init();

	sei();

	init(); //Initializeert LCD scherm in 4 bit mode

	Lcd_WriteFirstLine("counter:         "); 
	while(1){
		char str[5];
		sprintf(str,"%d    ", counter);
		Lcd_WriteSecondLine(str);
	}
}

