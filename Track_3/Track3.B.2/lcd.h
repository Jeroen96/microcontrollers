/*
 * lcd.h
 *
 * Created: 26-2-2016 11:48:36
 *  Author: guusv_000
 */ 

#ifndef LCD_H_
#define LCD_H_

extern void init();
extern void display_text(unsigned char dat);
extern void lcd_writeChar(unsigned char dat);
extern void set_cursor(int position);



#endif /* LCD_H_ */