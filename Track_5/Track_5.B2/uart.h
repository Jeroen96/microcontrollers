/*
 * uart.h
 *
 * Created: 15-3-2016 11:18:22
 *  Author: Jeroen
 */ 


#ifndef UART_H_
#define UART_H_


#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define LF			0x0a					// ascii code for Line Feed
#define CR			0x0d					// ascii code for Carriage Return
#define BIT(x)		(1 << (x))
#define UART0_BAUD	2400					// Baud rate USART0
#define MYUBRR		F_CPU/16/UART0_BAUD - 1	// My USART Baud Rate Registerchar character;


 void usart0_init( void );				// initialize USART0 - receive/transmit
 void usart0_start( void );			    // UART0 receiver & transmitter enable
 int uart0_sendChar( char ch );         // UART0: receive ch
 char uart0_receiveChar( void );        // UART0: send ch
 int uart0_receiveString(char* string); // UART0: receive string until LF

#endif /* UART_H_ */