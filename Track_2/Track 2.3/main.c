#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
unsigned int counter;

typedef struct { 
	unsigned char data;
	unsigned int delay ;
} PATTERN_STRUCT; 

// 7 seg
// PORTD dp G F E D C B A
//        y y y y x x x x

PATTERN_STRUCT pattern[] = { 
	{0x06, 150}, {0x5B, 150}, 
	{0x4F, 150}, {0x66, 150},
	{0x6D, 150}, {0x7D, 150}, {0x07, 150}, {0x7F, 150},
	{0x6F, 150}, {0x77, 150}, {0x7C, 150}, {0x39, 150},
	{0x5E, 150}, {0x79, 150} 
};

/******************************************************************/
void wait( int ms )
/* 
short:			Busy wait number of millisecs
inputs:			int ms (Number of millisecs to busy wait)
outputs:	
notes:			Busy wait, not very accurate. Make sure (external)
				clock value is set. This is used by _delay_ms inside
				util/delay.h
Version :    	DMK, Initial code
*******************************************************************/
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );		// library function (max 30 ms at 8MHz)
	}
}

ISR( INT0_vect )
/*
short:			ISR INT0
inputs:
outputs:
notes:			Set PORTD.5
Version :    	DMK, Initial code
*******************************************************************/
{
	counter = counter + 1;
	if (counter > 13 || counter < 0){
		PORTC = 0x79;
	} else {
		PORTC = pattern[counter].data;
	}
	wait(100);
}

/******************************************************************/
ISR( INT1_vect )
/*
short:			ISR INT1
inputs:
outputs:
notes:			Clear PORTD.5
Version :    	DMK, Initial code
*******************************************************************/
{
	counter = counter - 1;
	if (counter > 13 || counter < 0){
		PORTC = 0x79;
	} else {
		PORTC = pattern[counter].data;
	}
	wait(100);
}



/******************************************************************/
int main( void )
/* 
short:			main() loop, entry point of executable
inputs:			
outputs:	
notes:			
Version :    	DMK, Initial code
*******************************************************************/
{
	DDRC = 0b11111111;					// PORTC all output 
	DDRD = 0x00; //PORTD all input

	EICRA |= 0x0B;			// INT1 falling edge, INT0 rising edge
	EIMSK |= 0x03;			// Enable INT1 & INT0

	sei();
	
	while (1==1)
	{
		
	}

	return 1;
}