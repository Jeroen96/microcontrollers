#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

typedef struct { 
	unsigned char data;
	unsigned int delay ;
} PATTERN_STRUCT; 

// 7 seg
// PORTD dp G F E D C B A
//        y y y y x x x x

PATTERN_STRUCT pattern[] = { 
	{0x01, 1000}, {0x02, 1000}, {0x40, 1000}, {0x10, 1000}, {0x08, 1000}, {0x04, 1000}, {0x40, 1000}, {0x20, 1000}
};

/******************************************************************/
void wait( int ms )
/* 
short:			Busy wait number of millisecs
inputs:			int ms (Number of millisecs to busy wait)
outputs:	
notes:			Busy wait, not very accurate. Make sure (external)
				clock value is set. This is used by _delay_ms inside
				util/delay.h
Version :    	DMK, Initial code
*******************************************************************/
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );		// library function (max 30 ms at 8MHz)
	}
}


/******************************************************************/
int main( void )
/* 
short:			main() loop, entry point of executable
inputs:			
outputs:	
notes:			
Version :    	DMK, Initial code
*******************************************************************/
{
	int index = 0;
	DDRC = 0b11111111;					// PORTC all output 
	DDRD = 0x00; //PORTD all input

	EICRA |= 0x0B;			// INT1 falling edge, INT0 rising edge
	EIMSK |= 0x03;			// Enable INT1 & INT0

	sei();
	
	while (1==1)
	{
		PORTC = pattern[index].data;
		wait(pattern[index].delay);

		index++;
		if (index>7){
			index = 0;
		}

	}

	return 1;
}

